import * as React from "react";
import { Route, Routes } from "react-router-dom";
import Home from "./routes";
import About from "./routes/About";
import Signup from "./routes/Signup";

const App = () => {
  return <>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/signup" element={<Signup />} />
    </Routes>
  </>;
};

export default App;
